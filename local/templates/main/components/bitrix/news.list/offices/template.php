<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//workingTime, object, YM, numberOfPhones, address
//die('<pre>' . print_r($arResult, TRUE) . '</pre>');

$i = 0; foreach($arResult['ITEMS'] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

 if(isset($_POST['id of object']) AND (
   $arItem['DISPLAY_PROPERTIES']['object']['VALUE'] != '' AND
   !in_array($_POST['id of object'], $arItem['DISPLAY_PROPERTIES']['object']['VALUE'])
  )
 ){ continue; }

 //die('<pre>' . print_r($arItem, TRUE) . '</pre>');
?>
 <li class='map-card js-map-card swiper-slide' data-index='<?=$i?>'>
  <div class='map-card__image'>
   <picture>
    <img class='lazy-img' src='<?=$arItem['PREVIEW_PICTURE']['SRC']?>' width='529' height='331' alt='<?=$arItem['NAME']?>'> <!-- srcset='img/contacts-3@2x.jpg 2x' -->
   </picture>
  </div>
  <div class='map-card__text'>
   <h3><?=$arItem['NAME']?><br><?=$arItem['DISPLAY_PROPERTIES']['numberOfPhones']['VALUE'][0]?></h3>
   <p><?=$arItem['DISPLAY_PROPERTIES']['address']['VALUE']?></p>
   <p>Часы работы:</p>
<?=$arItem['DISPLAY_PROPERTIES']['workingTime']['~VALUE']?>
   <button type='button'>Выделить на карте
    <svg width='11' height='10' aria-hidden='true'><use xlink:href='img/sprite.svg#right-arrow'></use></svg>
   </button>
  </div>
  <script>
if(typeof locations === 'undefined'){ var locations = Array(); }
locations.push({
 title: '<?=$arItem['NAME']?>',
 position: [<?=$arItem['DISPLAY_PROPERTIES']['YM']['VALUE']?>],
 defaultUrl: '<?=$arItem['PREVIEW_PICTURE']['SRC']?>',
 activeUrl: '<?=$arItem['PREVIEW_PICTURE']['SRC']?>'
})
  </script>
 </li>
<?$i++; } unset($i); //endforeach;?>
<script>
document.addEventListener('DOMContentLoaded', function(){
 var addressCards = document.querySelectorAll('.js-map-card')

 var icon = {
  defaultSize: [40, 40],
  activeSize: [60, 60]
 }

 function init(){
  var map = new ymaps.Map('YMap', {
   center: [55.941712, 37.870026],
   zoom: 10,
   controls: []
  })

  function calcIconImageOffset(iconSize){
   return [-Math.abs(iconSize[0] / 2), -Math.abs(iconSize[1] / 2)];
  }

  var markers = locations.map(function(element){
   return new ymaps.Placemark(element.position, {}, {
    iconLayout: 'default#image',
    iconImageHref: element.defaultUrl,
    iconImageSize: icon.defaultSize,
    iconImageOffset: calcIconImageOffset(icon.defaultSize),
    zIndex: 1
   })
  })
  markers.forEach(function(marker){
   map.geoObjects.add(marker)
  })

  var addMarker = function addMarker(index){
   markers.forEach(function(marker, i){
    marker.options.set("iconImageHref", locations[i].defaultUrl)
    marker.options.set("iconImageSize", icon.defaultSize)
    marker.options.set("iconImageOffset", calcIconImageOffset(icon.defaultSize))
    markers[index].options.set("zIndex", 1)
   })
   markers[index].options.set("iconImageHref", locations[index].activeUrl)
   markers[index].options.set("iconImageSize", icon.activeSize)
   markers[index].options.set("iconImageOffset", calcIconImageOffset(icon.activeSize))
   markers[index].options.set("zIndex", 10)
   map.panTo(markers[index].geometry._coordinates)
  };

  addressCards.forEach(function(addressCard){
   addressCard.addEventListener("click", function(){
    addressCards.forEach(function(card){
     card.classList.remove("active")
    })
    addressCard.classList.add("active")
    addMarker(addressCard.getAttribute("data-index"))
   })
  })
 }

 function yandexMaps(){
  ymaps.ready(init)
 }

 yandexMaps()
})
</script>
