<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== TRUE) die();
$this->setFrameMode($arResult, TRUE);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');

foreach($arResult['ITEMS'] AS $arItem){
 //die('<pre>' . print_r($arItem, TRUE) . '</pre>');
?>
   <li class='profit-card<?=' ' . $arItem['PROPERTIES']['className']['VALUE']?>'>
    <picture>
     <img alt='<?=$arItem['NAME']?>'
          class='lazy-img'
          src='<?=$arItem['DISPLAY_PROPERTIES']['icon']['FILE_VALUE']['SRC']?>'
          srcset='<?=$arItem['DISPLAY_PROPERTIES']['srcSet2x']['FILE_VALUE']['SRC']?>'
          width='69' height='69'
     />
    </picture>
    <p><?=preg_replace('#(\d+[\s\d+]*)#', '<b>$1</b>', $arItem['NAME'])?></p>
   </li>
  <?} //endforeach;?>