<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== TRUE) die();
$this->setFrameMode($arResult, TRUE);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');

foreach($arResult['ITEMS'] AS $arItem){
 $A = ' ' . str_replace('_', '-', $arItem['PROPERTIES']['typeOfMark']['VALUE_XML_ID']);
?>
 <li class='project-card swiper-slide js-map-card<?=($arItem['PROPERTIES']['mark']['VALUE'] != '' AND $A != ' ') ? $A : ''?>' data-index='<?=$arItem['SORT']?>'>
  <div class='project-card__image'>
   <a class='js-project-link' href='/object/<?=$arItem['CODE']?>'>
    <picture>
     <img class='lazy-img'
          src='<?=$arItem['PREVIEW_PICTURE']['SRC']?>'
          <?($arItem['PROPERTIES']['srcSet2x']['SRC'] != '') ? " srcset='{$arItem['PROPERTIES']['srcSet2x']['SRC']}'" : ''?>
          width='530' height='330'
          alt='<?=$arItem['NAME']?>'
     />
    </picture>
   </a>
  </div>
  <div class='project-card__text'>
   <a class='js-project-link' href='/object/<?=$arItem['CODE']?>'>
    <h3><?=$arItem['NAME']?></h3>
   </a>
   <p>От 2,5 млн. руб.</p>
<?if($arItem['PROPERTIES']['mark']['VALUE'] != ''){?>
   <div class='project-card__promo'>
    <span><?=$arItem['PROPERTIES']['mark']['VALUE']?></span>
   </div>
<?}?>
   <button type='button'>Показать на карте<svg width='11' height='10' aria-hidden='true'><use xlink:href='img/sprite.svg#right-arrow'></use></svg>
   </button>
  </div>
 </li>
<?} //endforeach;?>