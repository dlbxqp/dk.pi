<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();
$this->setFrameMode(true);

//die('<pre>' . print_r($arResult['ITEMS'], TRUE) . '</pre>');

foreach($arResult['ITEMS'] AS $arItem){
 $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
 $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

 if(!isset($aA[$arItem['PROPERTIES']['object']['VALUE']])){
  $aA[$arItem['PROPERTIES']['object']['VALUE']]['count'] = 1;
  $aA[$arItem['PROPERTIES']['object']['VALUE']]['name'] = $arItem['DISPLAY_PROPERTIES']['object']['LINK_ELEMENT_VALUE'][
   $arItem['PROPERTIES']['object']['VALUE']
  ]['NAME'];
 } else{
  $aA[$arItem['PROPERTIES']['object']['VALUE']]['count']++;
 }

 $aA[$arItem['PROPERTIES']['object']['VALUE']]['code'] = $arItem['DISPLAY_PROPERTIES']['object']['LINK_ELEMENT_VALUE'][$arItem['DISPLAY_PROPERTIES']['object']['VALUE']]['CODE'];
}

foreach($aA AS $k => $v){
 echo <<<HD
 <li>
  <a class='js-option'
     href='#'
     onClick='event.preventDefault(); changeCurrentSectionOfNews({$k})'
     data-countOfElements='{$v['count']}'
     data-code='{$v['code']}'
  >{$v['name']}</a>
 </li>
 
HD;
}
?>