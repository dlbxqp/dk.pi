<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== TRUE) die();
$this->setFrameMode($arResult, TRUE);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');
?>
<section class='about__partners'>
 <div class='js-section' id='partners' data-name='partners'></div>
 <h2>Партнёры</h2>
 <ul>
  <?foreach($arResult['ITEMS'] AS $arItem){?>
   <li style='width: calc(100% / 8 - 27px)'>
    <div class='about__partners-logo about__partners-logo--sberbank'>
     <img alt='<?=$arItem['NAME']?>'
          class='lazy-img'
          src='<?=$arItem['DISPLAY_PROPERTIES']['logotype']['FILE_VALUE']['SRC']?>'
     >
    </div>
   </li>
  <?} //endforeach;?>
 </ul>
</section>