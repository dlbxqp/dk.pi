<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== TRUE) die();
$this->setFrameMode($arResult, TRUE);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');
?>
<section class='about__social news js-social'>
 <div class='js-section' id='social' data-name='social'></div>
 <h2>Социальные объекты</h2>
 <div class='swiper-container'>
  <ul class='swiper-wrapper'>
  <?foreach($arResult['ITEMS'] AS $arItem){?>
   <li class='news-card news-card--social swiper-slide'>
    <picture>
     <img alt='<?=$arItem['NAME']?>'
          class='lazy-img'
          src='<?=$arItem['PREVIEW_PICTURE']['SRC']?>'
          srcset='<?=$arItem['PROPERTIES']['srcSet2x']['SRC']?>'
          width='270' height='170'
     />
    </picture>
    <h3><?=$arItem['NAME']?></h3>
   </li>
  <?} //endforeach;?>
  </ul>
 </div>
</section>