<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== TRUE) die();
$this->setFrameMode(TRUE);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');
?>
<section class='about__news news js-news'>
 <div class='js-section' id='news' data-name='news'></div>
 <h2>Новости</h2>
 <div class='swiper-container'>
  <ul class='swiper-wrapper'>
<?
foreach($arResult['ITEMS'] AS $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$a_ = explode('-', $arItem['DISPLAY_ACTIVE_FROM']);
	if((int)$a_[1] == 1){  $A = 'января'; }
	elseif((int)$a_[1] == 2){ $A = 'февраля'; }
 elseif((int)$a_[1] == 3){ $A = 'марта'; }
 elseif((int)$a_[1] == 4){ $A = 'апреля'; }
 elseif((int)$a_[1] == 5){ $A = 'мая'; }
 elseif((int)$a_[1] == 6){ $A = 'июня'; }
 elseif((int)$a_[1] == 7){ $A = 'июля'; }
 elseif((int)$a_[1] == 8){ $A = 'августа'; }
 elseif((int)$a_[1] == 9){ $A = 'сентября'; }
 elseif((int)$a_[1] == 10){ $A = 'октября'; }
 elseif((int)$a_[1] == 11){ $A = 'ноября'; }
 else{ $A = 'декабря'; }
 $A = "{$a_[2]} $A {$a_[0]}";
?>
 <li class='news-card swiper-slide'>
  <a href='<?=$arItem['DETAIL_PAGE_URL']?>'>
   <picture>
    <img class='lazy-img'
         src='<?=$arItem['PREVIEW_PICTURE']['SRC']?>'
         srcset='<?=$arItem['PROPERTIES']['srcSet2x']['SRC']?>'
         width='270' height='170'
         alt='<?=$arItem['NAME']?>'
    />
   </picture>
   <h3><?=$arItem['NAME']?></h3>
  </a>
  <p><?=$arItem['PREVIEW_TEXT']?>
   <a href='<?=$arItem['DETAIL_PAGE_URL']?>'>Подробнее
    <svg width='11' height='10' aria-hidden='true'><use xlink:href='img/sprite.svg#right-arrow'></use></svg>
   </a>
  </p>
  <time datetime='<?=$arItem['DISPLAY_ACTIVE_FROM']?>'><?=$A?></time>
 </li>
<?
} //endforeach;
unset($A, $a_);
?>
  </ul>
 </div>
 <a class='button' href='/news/'>Все новости</a>
</section>