<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();

if(!empty($arResult)){
 //die('<pre>' . print_r($arResult, TRUE) . '</pre>');
?>
<ul class='swiper-wrapper'>
<?
 foreach($arResult as $arItem){
  if($arParams['MAX_LEVEL'] == 1 && $arItem['DEPTH_LEVEL'] > 1){	continue; }
  //if($arItem['SELECTED']){ $A = ' header__item--active'; }
  echo " <li class='swiper-slide'><a class='js-anchor-link' href='/{$arItem['LINK']}' data-target='{$arItem['LINK']}'>{$arItem['TEXT']}</a></li>\r\n";
  //unset($A);
 } //endforeach
?>
</ul>
<?} //endif?>