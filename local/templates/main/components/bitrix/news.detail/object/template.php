<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(TRUE);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');

$_POST['id of object'] = $arResult['~ID'];
$_POST['name of object'] = $arResult['NAME'];
$_POST['description of object'] = $arResult['PREVIEW_TEXT'];
$_POST['mark of object'] = $arResult['DISPLAY_PROPERTIES']['mark']['VALUE'];
$_POST['srcSet2x of object'] = $arResult['DISPLAY_PROPERTIES']['srcSet2x']['FILE_VALUE']['SRC'];
$_POST['picture of object'] = $arResult['FIELDS']['PREVIEW_PICTURE']['SRC'];


//die('<pre>' . print_r($arResult['DISPLAY_PROPERTIES']['documents']['FILE_VALUE'], TRUE) . '</pre>');
unset($A);
foreach($arResult['DISPLAY_PROPERTIES']['documents']['FILE_VALUE'] AS $v){
 $v['FILE_SIZE'] = round($v['FILE_SIZE'] / 1024 / 1024, 2); //die('> ' . $v['FILE_SIZE']);

 $A .= <<<HD
<div class='files'>
 <div class='files__column'>
  <svg width='18' height='18' aria-hidden='true'><use xlink:href='img/sprite.svg#icon-report'></use></svg>
  <a href='{$v['SRC']}' download>{$v['ORIGINAL_NAME']}</a><!-- Бух отчетность 2 кв 2019.pdf -->
 </div>
 <div class='files__column'>
  <div class='files__date'>{$v['TIMESTAMP_X']}</div><!-- 01:00:43 01-08-2019 -->
  <span>{$v['FILE_SIZE']} Мб</span><!-- 1.76 -->
  <a href='{$v['SRC']}' aria-label='Скачать файл' download>
   <svg width='15' height='15' aria-hidden='true'><use xlink:href='img/sprite.svg#icon-download'></use></svg>
  </a>
  <a href='{$v['SRC']}' target='_blank' aria-label='Открыть файл в новом окне'>
   <svg width='15' height='15' aria-hidden='true'><use xlink:href='img/sprite.svg#icon-new-window'></use></svg>
  </a>
 </div>
</div>

HD;
}
$_POST['documentation of object'] = $A; unset($A);