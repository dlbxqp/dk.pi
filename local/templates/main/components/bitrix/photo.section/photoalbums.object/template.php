<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//die('<pre>' . print_r($arResult, TRUE) . '</pre>');
//die('> ' . print_r($GLOBALS, TRUE));

foreach($arResult["ROWS"] as $arItems){
 foreach($arItems as $arItem){
  if(is_array($arItem)){
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));
   #
   if($arItem['PROPERTIES']['object']['VALUE'] == $_POST['id of object']){
    $A = rand(0, count($arItem['DISPLAY_PROPERTIES']['photos']['FILE_VALUE']) - 1);
    $A = $arItem['DISPLAY_PROPERTIES']['photos']['FILE_VALUE'][$A]['SRC'];
    echo <<<HD
     <div class='progress__slide-image swiper-slide'>
      <img class='lazy-img' src='{$A}' alt='general-plan'> <!--srcset='img/object/february@2x.jpg 2x'-->
      <span>{$arItem['NAME']}</span>
     </div>

HD;
   }	unset($A);
  }
 }
}