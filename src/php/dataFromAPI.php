<?php
$url = 'https://pb2308.profitbase.ru/api/v4/json/';

$A = '{ "type": "api-app", "credentials": { "pb_api_key": "app-5dc42ec16ece3" } }';
#
$ch = curl_init();
$options = array(
 CURLOPT_URL => "{$url}authentication",
 CURLOPT_CUSTOMREQUEST => 'POST',
 CURLOPT_HTTPHEADER => array(
  'Content-Type: application/json',
  'Content-Length: ' . strlen($A)
 ),
 CURLOPT_POSTFIELDS => $A,
 CURLOPT_RETURNTRANSFER => TRUE //TRUE возвращает массив, FALSE - объект
); unset($A);
curl_setopt_array($ch, $options);

$aA = curl_exec($ch);
$aA = json_decode($aA, TRUE);
#
if(!isset($aA)){ die('(1) Нет доступа к API'); }
echo '<pre>' . print_r($aA, TRUE) . "</pre>\r\n";
#
$access_token = $aA['access_token'];
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/src/accessToken.txt', $access_token);


curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_URL, "{$url}projects?access_token={$access_token}");
$aA = curl_exec($ch);
$aA = json_decode($aA, TRUE);
#
if(!isset($aA)){ die('(2) Нет доступа к API'); }
//echo '<pre>' . print_r($aA, TRUE) . "</pre>\r\n";
#
//$aProjects = $aA;
foreach($aA AS $v){ $aProjects[$v['title']] = $v['id']; }
unset($aA);


//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_URL, "{$url}property?access_token={$access_token}");
$aA = curl_exec($ch);
$aA = json_decode($aA, TRUE);
#
if(!isset($aA)){ die('(3) Нет доступа к API'); }
//echo '<pre>' . print_r($aA, TRUE) . "</pre>\r\n";
#
$aApartmentsCount = Array();
foreach($aA['data'] AS $v){
 if(
  $v['status'] != 'AVAILABLE' OR
  $v['houseName'] == 'Паркинг' OR
  $v['houseName'] == 'Кладовые'
 ){ continue; }

 $v['idProject'] = $aProjects[$v['projectName']];
 $aProperties[] = $v;

 if(!isset($aApartmentsCount[$v['idProject']][$v['rooms_amount']])){
  $aApartmentsCount[$v['idProject']][$v['rooms_amount']]['count'] = 1;
  $aApartmentsCount[$v['idProject']][$v['rooms_amount']]['minPrice'] = $v['price']['value'];
 } else{
  $aApartmentsCount[$v['idProject']][$v['rooms_amount']]['count']++;
  if($aApartmentsCount[$v['idProject']][$v['rooms_amount']]['minPrice'] > $v['price']['value']){ $aApartmentsCount[$v['idProject']][$v['rooms_amount']]['minPrice'] = $v['price']['value']; }
 }

 $aB[] = $v['houseName'];
} unset($aA);
//echo '<pre>' . print_r($aProperties, TRUE) . "</pre>\r\n";
//echo count($aProperties);
$aB = array_unique($aB); reset($aB); echo '<pre>' . print_r($aB, TRUE) . "</pre>\r\n";
#
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/src/apartmentsCount.txt', count($aProperties));
#
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/src/apartmentsCount.json', json_encode($aApartmentsCount));

curl_close($ch);