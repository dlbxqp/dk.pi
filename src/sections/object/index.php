<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Объект');
$APPLICATION->AddViewContent('classNameOfTagMain', 'object', 1);

$APPLICATION->IncludeComponent(
 'bitrix:news.detail',
 'object',
 Array(
  'ACTIVE_DATE_FORMAT' => 'Y-m-d',
  'ADD_ELEMENT_CHAIN' => 'N',
  'ADD_SECTIONS_CHAIN' => 'Y',
  'AJAX_MODE' => 'N',
  'AJAX_OPTION_ADDITIONAL' => '',
  'AJAX_OPTION_HISTORY' => 'N',
  'AJAX_OPTION_JUMP' => 'N',
  'AJAX_OPTION_STYLE' => 'N',
  'BROWSER_TITLE' => '-',
  'CACHE_GROUPS' => 'N',
  'CACHE_TIME' => '36000000',
  'CACHE_TYPE' => 'A',
  'CHECK_DATES' => 'Y',
  'DETAIL_URL' => '',
  'DISPLAY_BOTTOM_PAGER' => 'N',
  'DISPLAY_DATE' => 'N',
  'DISPLAY_NAME' => 'Y',
  'DISPLAY_PICTURE' => 'Y',
  'DISPLAY_PREVIEW_TEXT' => 'Y',
  'DISPLAY_TOP_PAGER' => 'N',
  'ELEMENT_CODE' => $_REQUEST['id'],
  'ELEMENT_ID' => '',
  'FIELD_CODE' => array('PREVIEW_PICTURE', ''),
  'IBLOCK_ID' => 2,
  'IBLOCK_TYPE' => 'mainContent',
  'IBLOCK_URL' => '',
  'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y',
  'MESSAGE_404' => '',
  'META_DESCRIPTION' => '-',
  'META_KEYWORDS' => '-',
  'PAGER_BASE_LINK_ENABLE' => 'N',
  'PAGER_SHOW_ALL' => 'N',
  'PAGER_TEMPLATE' => '.default',
  'PAGER_TITLE' => 'Страница',
  'PROPERTY_CODE' => array('documents', 'mark', 'srcSet2x'),
  'SET_BROWSER_TITLE' => 'Y',
  'SET_CANONICAL_URL' => 'N',
  'SET_LAST_MODIFIED' => 'N',
  'SET_META_DESCRIPTION' => 'Y',
  'SET_META_KEYWORDS' => 'Y',
  'SET_STATUS_404' => 'N',
  'SET_TITLE' => 'Y',
  'SHOW_404' => 'N',
  'STRICT_SECTION_CHECK' => 'N',
  'USE_PERMISSIONS' => 'N',
  'USE_SHARE' => 'N'
 )
);
?>

<nav class='object__list anchor js-anchor-header swiper-container anchor--object' aria-label='Навигация по странице'>
 <button type='button'>
  <span><?=$_POST['name of object']?></span>
 </button>
 <ul class='swiper-wrapper'>
  <li class='swiper-slide'><a class='js-anchor-link' href='#about' data-target='about'>О проекте</a></li>
  <li class='swiper-slide'><a class='js-anchor-link' href='#choice' data-target='choice'>Выбор квартир</a></li>
  <li class='swiper-slide'><a class='js-anchor-link' href='#progress' data-target='progress'>Ход строительства</a></li>
  <li class='swiper-slide'><a class='js-anchor-link' href='#offer' data-target='offer'>Способы покупки</a></li>
  <li class='swiper-slide'><a class='js-anchor-link' href='#documents' data-target='documents'>Документы</a></li>
  <li class='swiper-slide'><a class='js-anchor-link' href='#address' data-target='address'>Расположение</a></li>
 </ul>
</nav>

<h1 class='visually-hidden'><?=$_POST['name of object']?></h1>
<section class='object-plan'>
 <h2 class='visually-hidden'>О комплексе <?=$_POST['name of object']?></h2>
 <div class='js-section' id='about' data-name='about'></div>
 <div class='object-plan__image'>
  <img class='lazy-img' src='<?=$_POST['picture of object']?>'<?if($_POST['srcSet2x of object'] != ''){ echo " srcset='{$_POST['srcSet2x of object']}'"; }?> alt='plan' width='471' height='250'>
<?if($_POST['mark of object'] != ''){ echo "<span>{$_POST['mark of object']}</span>"; }?>
 </div>
 <div class='object-plan__text'>
  <p><?=$_POST['description of object']?></p>
  <button type='button' onClick='pb_front_widget.show({projectId: <?=$_REQUEST['id']?>})'>
   <span>Выбрать квартиру</span>
  </button>
 </div>
</section>

<section class='object-general'>
 <h2>Генплан</h2>
 <div class='object-general__image'><img class='lazy-img' data-src='img/object/general-plan.jpg' data-srcset='img/object/general-plan@2x.jpg 2x' alt='general-plan' width='1129' height='634'></div>
</section>

<section class='object__choice choice'>
 <div class='js-section' id='choice' data-name='choice'></div>
 <div class='choice__list'>
  <h2>Выбор квартир</h2>
  <div class='swiper-container'>
   <ul class='swiper-wrapper'>
    <li class='swiper-slide'><a href='#'>3D тур по квартирам</a></li>
    <li class='swiper-slide'><a href='#'>3D тур по входным группам</a></li>
   </ul>
  </div>
 </div>
 <div class='choice__wrap'>
  <div class='choice__apartments'>
<?
$apartmentsCount = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/src/apartmentsCount.json');
$aApartmentsCount = json_decode($apartmentsCount, TRUE);
ksort($aApartmentsCount[$_REQUEST['id']]);
foreach($aApartmentsCount[$_REQUEST['id']] AS $k => $v){
 if((int)$k == 1){
  $A = "<svg width='52' height='71' aria-hidden='true'><use xlink:href='img/sprite.svg#choice_one_room'></use></svg><svg width='69' height='100' aria-hidden='true'><use xlink:href='img/sprite.svg#choicePlanOneRoom'></use></svg>";
 } elseif((int)$k == 2){
  $A = "<svg width='99' height='69' aria-hidden='true'><use xlink:href='img/sprite.svg#choice_two_rooms'></use></svg><svg width='106' height='104' aria-hidden='true'><use xlink:href='img/sprite.svg#choicePlanTwoRooms'></use></svg>";
 } else{
  $A = "<svg width='97' height='90' aria-hidden='true'><use xlink:href='img/sprite.svg#choice_tree_rooms'></use></svg><svg width='119' height='118' aria-hidden='true'><use xlink:href='img/sprite.svg#choicePlanTreeRooms'></use></svg>";
 }

 $B = round($v['minPrice'] / 1000000, 2);

 echo <<<HD
<div class='choice__apartment'>
 <div class='choice__apartment__wrapper'>
  <div class='choice__apartment__images'>{$A}</div>
  <span>{$k} комнатные квартиры от {$B} млн руб.</span>
 </div>
</div>

HD;
}
?>
  </div>
  <div class='choice__btn'>
   <button type='button' onClick='pb_front_widget.show({projectId: 4668})'>Выбрать квартиру</button>
  </div>
 </div>
</section>

 <section class='object__progress progress'>
  <div class='js-section' id='progress' data-name='progress'></div>
  <div class='progress__list'>
   <div class='progress__title'>
    <h2>Ход строительства</h2>
   </div>
   <div class='swiper-container progress__slider-container'>
    <ul class='progress__menu swiper-wrapper'>
     <li class='progress__item progress__item--active swiper-slide'><a href='#'>Фото</a></li>
     <li class='progress__item swiper-slide'><a href='#'>Веб-камера</a></li>
     <li class='progress__item swiper-slide'><a href='#'>Аэрофотосъёмка</a></li>
    </ul>
   </div>
  </div>
  <div class='progress__slider'>
   <div class='progress__slider-controls'>
    <button class='progress__slider-control swiper-button-prev'>
     <svg width='17' height='14' aria-hidden='true'><use xlink:href='img/sprite.svg#left-arrow'></use></svg>
    </button>
    <button class='progress__slider-control swiper-button-next'>
     <svg width='17' height='14' aria-hidden='true'><use xlink:href='img/sprite.svg#right-arrow'></use></svg>
    </button>
   </div>
   <div class='progress__slider-container swiper-container js-slider-container'>
    <div class='swiper-wrapper'>
<?
$APPLICATION->IncludeComponent('bitrix:photo.section', 'photoalbums.object', Array(
 'ADD_SECTIONS_CHAIN' => 'Y',	// Включать раздел в цепочку навигации
	'AJAX_MODE' => 'N',	// Включить режим AJAX
	'AJAX_OPTION_ADDITIONAL' => '',	// Дополнительный идентификатор
	'AJAX_OPTION_HISTORY' => 'N',	// Включить эмуляцию навигации браузера
	'AJAX_OPTION_JUMP' => 'N',	// Включить прокрутку к началу компонента
	'AJAX_OPTION_STYLE' => 'N',	// Включить подгрузку стилей
	'BROWSER_TITLE' => '-',	// Установить заголовок окна браузера из свойства
	'CACHE_FILTER' => 'N',	// Кешировать при установленном фильтре
	'CACHE_GROUPS' => 'N',	// Учитывать права доступа
	'CACHE_TIME' => '36000000',	// Время кеширования (сек.)
	'CACHE_TYPE' => 'A',	// Тип кеширования
	'DETAIL_URL' => '',	// URL, ведущий на страницу с содержимым элемента раздела
	'DISPLAY_BOTTOM_PAGER' => 'N',	// Выводить под списком
	'DISPLAY_TOP_PAGER' => 'N',	// Выводить над списком
	'ELEMENT_SORT_FIELD' => 'sort',	// По какому полю сортируем фотографии
	'ELEMENT_SORT_ORDER' => 'asc',	// Порядок сортировки фотографий в разделе
	'FIELD_CODE' => array(	// Поля
			0 => '',
			1 => '',
		),
		'FILTER_NAME' => '',	// Имя массива со значениями фильтра для фильтрации элементов
		'IBLOCK_ID' => '7',	// Инфоблок
		'IBLOCK_TYPE' => 'mainContent',	// Тип инфоблока
		'LINE_ELEMENT_COUNT' => '4',	// Количество фотографий, выводимых в одной строке таблицы
		'MESSAGE_404' => '',	// Сообщение для показа (по умолчанию из компонента)
		'META_DESCRIPTION' => '-',	// Установить описание страницы из свойства
		'META_KEYWORDS' => '-',	// Установить ключевые слова страницы из свойства
		'PAGER_BASE_LINK_ENABLE' => 'N',	// Включить обработку ссылок
		'PAGER_DESC_NUMBERING' => 'N',	// Использовать обратную навигацию
		'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',	// Время кеширования страниц для обратной навигации
		'PAGER_SHOW_ALL' => 'N',	// Показывать ссылку 'Все'
		'PAGER_SHOW_ALWAYS' => 'N',	// Выводить всегда
		'PAGER_TEMPLATE' => '.default',	// Шаблон постраничной навигации
		'PAGER_TITLE' => 'Фотографии',	// Название категорий
		'PAGE_ELEMENT_COUNT' => '4',	// Количество элементов на странице
		'PROPERTY_CODE' => array(	// Свойства
			0 => 'photos',
   1 => 'object'
		),
		'SECTION_CODE' => '',	// Код раздела
		'SECTION_ID' => 4,	// ID раздела
		'SECTION_URL' => '',	// URL, ведущий на страницу с содержимым раздела
		'SECTION_USER_FIELDS' => array(	// Свойства раздела
			0 => '',
			1 => '',
		),
		'SET_LAST_MODIFIED' => 'N',	// Устанавливать в заголовках ответа время модификации страницы
		'SET_STATUS_404' => 'N',	// Устанавливать статус 404
		'SET_TITLE' => 'N',	// Устанавливать заголовок страницы
		'SHOW_404' => 'N',	// Показ специальной страницы
	),
	false
);
?>
    </div>
   </div>
  </div>
 </section>
 <section class='object__news news js-news'>
  <h2>Новости жилого комплекса</h2>
  <div class='swiper-container'>
   <ul class='swiper-wrapper'>
<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'news.object', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
 'AJAX_MODE' => 'N', // Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
 'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N', // Учитывать права доступа
 'CACHE_TIME' => '36000000', // Время кеширования (сек.)
 'CACHE_TYPE' => 'A', // Тип кеширования
 'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
 'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
 'DISPLAY_DATE' => 'Y', // Выводить дату элемента
 'DISPLAY_NAME' => 'Y', // Выводить название элемента
 'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
 'FIELD_CODE' => array(0 => ''),
 'FILTER_NAME' => '', // Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 1, // Код информационного блока
 'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
 'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => 999, // Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
 'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
 'PAGER_TITLE' => 'Новости', // Название категорий
 'PARENT_SECTION' => '', // ID раздела
 'PARENT_SECTION_CODE' => '', // Код раздела
 'PREVIEW_TRUNCATE_LEN' => 0, // Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array(0 => 'object'),
 'SET_BROWSER_TITLE' => 'Y', // Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'Y', // Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'Y', // Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N', // Устанавливать статус 404
 'SET_TITLE' => 'Y', // Устанавливать заголовок страницы
 'SHOW_404' => 'N', // Показ специальной страницы
 'SORT_BY1' => 'ACTIVE_FROM', // Поле для первой сортировки новостей
 'SORT_BY2' => 'SORT', // Поле для второй сортировки новостей
 'SORT_ORDER1' => 'DESC', // Направление для первой сортировки новостей
 'SORT_ORDER2' => 'ASC', // Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
 'COMPONENT_TEMPLATE' => '.default'
 ),
 false
);
?>
   </ul>
  </div><a class='button' href='/news/?s=<?=(int)$_GET['id']?>'>Все новости жилого комплекса</a>
 </section>

 <section class='object__offer offer'>
  <div class='js-section' id='offer' data-name='offer'></div>
  <div class='offer__title'>
   <h2>Способы покупки</h2>
  </div>
  <div class='offer__wrap'>
   <div class='offer__item'>
    <img class='lazy-img'
         src='img/object/offer-img-1.jpg'
         srcset='img/object/offer-img-1@2x.jpg 2x'
         alt='img'
         width='368' height='222'
    />
    <span class='offer__first'>Ипотека от 6,5%</span>
    <button type='button'>Отправить заявку</button>
   </div>
   <div class='offer__item'>
    <img class='lazy-img'
         src='img/object/offer-img-2.jpg'
         srcset='img/object/offer-img-2@2x.jpg 2x'
         alt='img'
         width='368' height='222'
    />
    <span>Рассрочка</span>
    <button type='button'>Проконсультироваться</button>
   </div>
   <div class='offer__item'>
    <img class='lazy-img'
         src='img/object/offer-img-3.jpg'
         srcset='img/object/offer-img-3@2x.jpg 2x'
         alt='img'
         width='368' height='222'
    />
    <span>Материнский капитал</span>
    <button class='offer__last' type='button'>Узнать больше</button>
   </div>
  </div>
 </section>

 <section class='documents object__documents'>
  <div class='js-section' id='documents' data-name='documents'></div>
  <div class='documents__text'>
   <div class='documents__title'>
    <h2>Документы</h2>
   </div>
   <div class='documents__link'>
    <a href='#' target='_blank'>Посмотреть документы на наш.дом.рф</a>
    <svg width='17' height='14' aria-hidden='true'><use xlink:href='img/sprite.svg#right-arrow'></use></svg>
   </div>
  </div>
  <div class='documents__wrap'>
   <div class='documents__column'>
<?=$_POST['documentation of object']?>
   </div>
  </div><button type='button'>Посмотреть ещё</button>
 </section>

 <section class='address object__address js-contacts-slider'>
  <div class='js-section' id='address' data-name='address'></div>
  <h2>Расположение</h2>
  <div class='address__projects-map js-object-map'>
   <div class='address__map-cards'>
    <div class='swiper-container'>
     <ul class='swiper-wrapper'>
<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'offices', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
 'AJAX_MODE' => 'N', // Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
 'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N', // Учитывать права доступа
 'CACHE_TIME' => '36000000', // Время кеширования (сек.)
 'CACHE_TYPE' => 'A', // Тип кеширования
 'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
 'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
 'DISPLAY_DATE' => 'Y', // Выводить дату элемента
 'DISPLAY_NAME' => 'Y', // Выводить название элемента
 'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
 'FIELD_CODE' => array(0 => ''),
 'FILTER_NAME' => '', // Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 5, // Код информационного блока
 'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
 'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => 9, // Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
 'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
 'PAGER_TITLE' => 'Новости', // Название категорий
 'PARENT_SECTION' => '', // ID раздела
 'PARENT_SECTION_CODE' => '', // Код раздела
 'PREVIEW_TRUNCATE_LEN' => 0, // Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array('workingTime', 'object', 'YM', 'numberOfPhones', 'address'),
 'SET_BROWSER_TITLE' => 'Y', // Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'Y', // Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'Y', // Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N', // Устанавливать статус 404
 'SET_TITLE' => 'Y', // Устанавливать заголовок страницы
 'SHOW_404' => 'N', // Показ специальной страницы
 'SORT_BY1' => 'SORT', // Поле для первой сортировки новостей
 'SORT_BY2' => '', // Поле для второй сортировки новостей
 'SORT_ORDER1' => 'ASC', // Направление для первой сортировки новостей
 'SORT_ORDER2' => '', // Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
 'COMPONENT_TEMPLATE' => '.default'
 ),
 false
);
?>
     </ul>
    </div>
   </div>
   <div class='address__map-wrapper js-object-map' id='YMap'></div><!-- object-page-map -->
  </div>
 </section>

<?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');?>