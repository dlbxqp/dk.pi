<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Контакты');
$APPLICATION->AddViewContent('classNameOfTagMain', 'contacts-page', 1);
?>

<div class="contacts-page__wrapper js-contacts-slider">
 <h1>Контакты</h1>
 <div class="contacts-page__projects-map js-contacts-map">
  <div class="contacts-page__map-cards">
   <h2 class="visually-hidden">Все офисы Профи-инвест</h2>
   <div class="swiper-container">
    <ul class="swiper-wrapper">
<?
     $APPLICATION->IncludeComponent('bitrix:news.list', 'offices', Array(
      'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
      'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
      'AJAX_MODE' => 'N', // Включить режим AJAX
      'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
      'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
      'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
      'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
      'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
      'CACHE_GROUPS' => 'N', // Учитывать права доступа
      'CACHE_TIME' => '36000000', // Время кеширования (сек.)
      'CACHE_TYPE' => 'A', // Тип кеширования
      'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
      'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
      'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
      'DISPLAY_DATE' => 'Y', // Выводить дату элемента
      'DISPLAY_NAME' => 'Y', // Выводить название элемента
      'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
      'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
      'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
      'FIELD_CODE' => array(0 => ''),
      'FILTER_NAME' => '', // Фильтр
      'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
      'IBLOCK_ID' => 5, // Код информационного блока
      'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
      'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
      'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
      'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
      'NEWS_COUNT' => 9, // Количество новостей на странице
      'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
      'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
      'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
      'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
      'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
      'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
      'PAGER_TITLE' => 'Новости', // Название категорий
      'PARENT_SECTION' => '', // ID раздела
      'PARENT_SECTION_CODE' => '', // Код раздела
      'PREVIEW_TRUNCATE_LEN' => 0, // Максимальная длина анонса для вывода (только для типа текст)
      'PROPERTY_CODE' => array('workingTime', 'object', 'YM', 'numberOfPhones', 'address'),
      'SET_BROWSER_TITLE' => 'Y', // Устанавливать заголовок окна браузера
      'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
      'SET_META_DESCRIPTION' => 'Y', // Устанавливать описание страницы
      'SET_META_KEYWORDS' => 'Y', // Устанавливать ключевые слова страницы
      'SET_STATUS_404' => 'N', // Устанавливать статус 404
      'SET_TITLE' => 'Y', // Устанавливать заголовок страницы
      'SHOW_404' => 'N', // Показ специальной страницы
      'SORT_BY1' => 'SORT', // Поле для первой сортировки новостей
      'SORT_BY2' => '', // Поле для второй сортировки новостей
      'SORT_ORDER1' => 'ASC', // Направление для первой сортировки новостей
      'SORT_ORDER2' => '', // Направление для второй сортировки новостей
      'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
      'COMPONENT_TEMPLATE' => '.default'
     ),
      false
     );
?>
    </ul>
   </div>
  </div>
  <div class='contacts-page__map-wrapper js-contacts-map' id='YMap'></div>
 </div>
</div>

<?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');?>