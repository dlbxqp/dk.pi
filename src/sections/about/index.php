<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('О компании');
$APPLICATION->AddViewContent('classNameOfTagMain', 'about', 1);
?>

 <h1 class='visually-hidden'>О компании</h1>
 <nav class='about__anchor anchor js-anchor-header' aria-label='Навигация по странице'>
  <ul class="swiper-wrapper">
   <li class='swiper-slide'><a class="js-anchor-link" href="#about" data-target="about">О компании</a></li>
   <li class='swiper-slide'><a class="js-anchor-link" href="#profit" data-target="profit">Наши преимущества</a></li>
   <li class='swiper-slide'><a class="js-anchor-link" href="#ready" data-target="ready">Сданные объекты</a></li>
   <li class='swiper-slide'><a class="js-anchor-link" href="#social" data-target="social">Социальные объекты</a></li>
   <li class='swiper-slide'><a class="js-anchor-link" href="#partners" data-target="partners">Партнёры</a></li>
   <li class='swiper-slide'><a class="js-anchor-link" href="#news" data-target="news">Новости</a></li>
  </ul>
  <span class='swiper-notification' aria-live='assertive' aria-atomic='true'></span>
 </nav>

 <section class='about__intro'>
  <div class='js-section' id='about' data-name='about'></div>
  <h2 class='visually-hidden'>О компании</h2>
  <div class='about__intro-text about__intro-text--left'>
   <div class='about__intro-img'>
    <picture><img class='lazy-img' data-src='img/project-1@1x.jpg' data-srcset='img/project-1@2x.jpg 2x' width='345' height='219' alt='Проект компании'></picture>
   </div>
   <div class='about__wrapper'>
    <p>ООО «ПРОФИ-ИНВЕСТ» — предприятие, осуществляющее свою производственную деятельность в качестве Инвестора, Застройщика, Заказчика (Технического Заказчика), Генерального Подрядчика и Подрядчика, имеет Свидетельство № 0234.1-201050006011784 от 22 апреля 2010 года о допуске к работам, которые оказывают влияние на безопасность объектов капитального строительства зданий и сооружений жилищно-гражданского и промышленного назначения, выполняет весь комплекс строительно-монтажных работ с «нуля» и «под ключ».Основной специализацией ООО «ПРОФИ-ИНВЕСТ» является строительство зданий с монолитным железобетонным каркасом, монтаж внутренних и наружных инженерных систем и комплексное благоустройство, включая строительство дорог.</p>
   </div>
  </div>
  <div class='about__intro-text about__intro-text--right'>
   <div class='about__wrapper'>
    <p>В совершенстве освоенная технология строительных работ, комплексный подход к возведению объектов, знания и опыт применения инновационных методов строительства и современных материалов оборудования и техники, позволяют реализовывать с неизменным качеством и скоростью любые, даже самые сложные архитектурные задумки и проекты. Наличие серьезной материально-технической базы и налаженные долгосрочные связи с партнерами гарантируют Заказчикам реализацию проектов четко по графику без срывов и форс-мажорных обстоятельств.</p>
    <a class='button' href='#' onClick='pb_front_widget.show({accountId: 2308}); return false'>Выбрать квартиру</a>
   </div>
   <div class='about__intro-img'>
    <picture><img class='lazy-img' data-src='img/project-2@1x.jpg' data-srcset='img/project-2@2x.jpg 2x' width='345' height='219' alt='Проект компании'></picture>
   </div>
  </div>
 </section>

 <section class="about__profit">
  <div class="js-section" id="profit" data-name="profit"></div>
  <h2>Наши преимущества</h2>
  <ul>
<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'our_advantage', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d',	// Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y',	// Включать раздел в цепочку навигации
 'AJAX_MODE' => 'N',	// Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '',	// Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N',	// Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N',	// Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N',	// Включить подгрузку стилей
 'CACHE_FILTER' => 'N',	// Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N',	// Учитывать права доступа
 'CACHE_TIME' => '36000000',	// Время кеширования (сек.)
 'CACHE_TYPE' => 'A',	// Тип кеширования
 'CHECK_DATES' => 'Y',	// Показывать только активные на данный момент элементы
 'DETAIL_URL' => '',	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N',	// Выводить под списком
 'DISPLAY_DATE' => 'N',	// Выводить дату элемента
 'DISPLAY_NAME' => 'Y',	// Выводить название элемента
 'DISPLAY_PICTURE' => 'Y',	// Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'N',	// Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N',	// Выводить над списком
 'FIELD_CODE' => array(	// Поля
  0 => '',
  1 => '',
 ),
 'FILTER_NAME' => '',	// Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N',	// Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 6,	// Код информационного блока
 'IBLOCK_TYPE' => 'mainContent',	// Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y',	// Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y',	// Показывать элементы подразделов раздела
 'MESSAGE_404' => '',	// Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => 8,	// Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N',	// Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N',	// Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',	// Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N',	// Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N',	// Выводить всегда
 'PAGER_TEMPLATE' => '.default',	// Шаблон постраничной навигации
 'PAGER_TITLE' => 'Партнёры',	// Название категорий
 'PARENT_SECTION' => '',	// ID раздела
 'PARENT_SECTION_CODE' => '',	// Код раздела
 'PREVIEW_TRUNCATE_LEN' => '',	// Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array(	// Свойства
  0 => 'srcSet2x',
  1 => 'icon',
  2 => 'className'
 ),
 'SET_BROWSER_TITLE' => 'N',	// Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N',	// Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'N',	// Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'N',	// Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N',	// Устанавливать статус 404
 'SET_TITLE' => 'N',	// Устанавливать заголовок страницы
 'SHOW_404' => 'N',	// Показ специальной страницы
 'SORT_BY1' => 'SORT',	// Поле для первой сортировки новостей
 'SORT_BY2' => '',	// Поле для второй сортировки новостей
 'SORT_ORDER1' => 'ASC',	// Направление для первой сортировки новостей
 'SORT_ORDER2' => '',	// Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N',	// Строгая проверка раздела для показа списка
),
 false
);
?>
  </ul>
 </section>

 <section class='about__ready'>
  <div class='js-section' id='ready' data-name='ready'></div>
  <h2>Сданные объекты</h2>
  <ul>
<!--
   <li class='project-card project-card--ready'>
    <div class='project-card__image'>
     <picture><img class='lazy-img' data-src='img/project-1@1x.jpg' data-srcset='img/project-1@2x.jpg 2x' width='529' height='331' alt='ЖК Ривер Парк в г. Королёв'></picture>
    </div>
    <div class='project-card__text'>
     <h3>ЖК Ривер Парк в г. Королёв</h3>
     <p>Сдан в 2018 году</p>
    </div>
   </li>
   <li class='project-card project-card--ready'>
    <div class='project-card__image'>
     <picture><img class='lazy-img' data-src='img/project-2@1x.jpg' data-srcset='img/project-2@2x.jpg 2x' width='529' height='331' alt='ЖК 31 Квартал в г. Пушкино'></picture>
    </div>
    <div class='project-card__text'>
     <h3>ЖК 31 Квартал в г. Пушкино</h3>
     <p>Сдан в 2016 году</p>
    </div>
   </li>
   <li class='project-card project-card--ready'>
    <div class='project-card__image'>
     <picture><img class='lazy-img' data-src='img/project-3@1x.jpg' data-srcset='img/project-3@2x.jpg 2x' width='529' height='331' alt='Жилой дом Правда-4 в п.Правдинском'></picture>
    </div>
    <div class='project-card__text'>
     <h3>Жилой дом Правда-4 в п.Правдинском</h3>
     <p>Сдан в 2015 году</p>
    </div>
   </li>
   <li class='project-card project-card--ready'>
    <div class='project-card__image'>
     <picture><img class='lazy-img' data-src='img/project-4@1x.jpg' data-srcset='img/project-4@2x.jpg 2x' width='529' height='331' alt='ЖК на Тургенева 13 в г. Пушкино'></picture>
    </div>
    <div class='project-card__text'>
     <h3>ЖК на Тургенева 13 в г. Пушкино</h3>
     <p>Сдан в 2014 году</p>
    </div>
   </li>
  </ul>
-->
<?
   $APPLICATION->IncludeComponent('bitrix:news.list', 'objects.about', Array(
    'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
    'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
    'AJAX_MODE' => 'N', // Включить режим AJAX
    'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
    'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
    'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
    'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
    'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
    'CACHE_GROUPS' => 'N', // Учитывать права доступа
    'CACHE_TIME' => '36000000', // Время кеширования (сек.)
    'CACHE_TYPE' => 'A', // Тип кеширования
    'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
    'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
    'DISPLAY_DATE' => 'Y', // Выводить дату элемента
    'DISPLAY_NAME' => 'Y', // Выводить название элемента
    'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
    'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
    'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
    'FIELD_CODE' => array(0 => ''),
    'FILTER_NAME' => '', // Фильтр
    'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
    'IBLOCK_ID' => 2, // Код информационного блока
    'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
    'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
    'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
    'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
    'NEWS_COUNT' => 9, // Количество новостей на странице
    'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
    'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
    'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
    'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
    'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
    'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
    'PAGER_TITLE' => 'Новости', // Название категорий
    'PARENT_SECTION' => '', // ID раздела
    'PARENT_SECTION_CODE' => '', // Код раздела
    'PREVIEW_TRUNCATE_LEN' => 250, // Максимальная длина анонса для вывода (только для типа текст)
    'PROPERTY_CODE' => array(0 => 'srcSync2x'),
    'SET_BROWSER_TITLE' => 'N', // Устанавливать заголовок окна браузера
    'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
    'SET_META_DESCRIPTION' => 'N', // Устанавливать описание страницы
    'SET_META_KEYWORDS' => 'N', // Устанавливать ключевые слова страницы
    'SET_STATUS_404' => 'N', // Устанавливать статус 404
    'SET_TITLE' => 'N', // Устанавливать заголовок страницы
    'SHOW_404' => 'N', // Показ специальной страницы
    'SORT_BY1' => 'SORT', // Поле для первой сортировки новостей
    'SORT_BY2' => '', // Поле для второй сортировки новостей
    'SORT_ORDER1' => 'ASC', // Направление для первой сортировки новостей
    'SORT_ORDER2' => '', // Направление для второй сортировки новостей
    'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
    'COMPONENT_TEMPLATE' => '.default'
   ),
    false
   );
?>
  <!--<button class='button' type='button'>Показать еще</button>-->
 </section>

<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'social_objects', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d',	// Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y',	// Включать раздел в цепочку навигации
 'AJAX_MODE' => 'N',	// Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '',	// Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N',	// Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N',	// Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N',	// Включить подгрузку стилей
 'CACHE_FILTER' => 'N',	// Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N',	// Учитывать права доступа
 'CACHE_TIME' => '36000000',	// Время кеширования (сек.)
 'CACHE_TYPE' => 'A',	// Тип кеширования
 'CHECK_DATES' => 'Y',	// Показывать только активные на данный момент элементы
 'DETAIL_URL' => '',	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N',	// Выводить под списком
 'DISPLAY_DATE' => 'N',	// Выводить дату элемента
 'DISPLAY_NAME' => 'Y',	// Выводить название элемента
 'DISPLAY_PICTURE' => 'Y',	// Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'N',	// Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N',	// Выводить над списком
 'FIELD_CODE' => array(	// Поля
  0 => '',
  1 => '',
 ),
 'FILTER_NAME' => '',	// Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N',	// Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 4,	// Код информационного блока
 'IBLOCK_TYPE' => 'mainContent',	// Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y',	// Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y',	// Показывать элементы подразделов раздела
 'MESSAGE_404' => '',	// Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => 6,	// Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N',	// Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N',	// Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',	// Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N',	// Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N',	// Выводить всегда
 'PAGER_TEMPLATE' => '.default',	// Шаблон постраничной навигации
 'PAGER_TITLE' => 'Партнёры',	// Название категорий
 'PARENT_SECTION' => '',	// ID раздела
 'PARENT_SECTION_CODE' => '',	// Код раздела
 'PREVIEW_TRUNCATE_LEN' => '',	// Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array(	// Свойства
  0 => 'srcSet2x',
  1 => '',
 ),
 'SET_BROWSER_TITLE' => 'N',	// Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N',	// Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'N',	// Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'N',	// Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N',	// Устанавливать статус 404
 'SET_TITLE' => 'N',	// Устанавливать заголовок страницы
 'SHOW_404' => 'N',	// Показ специальной страницы
 'SORT_BY1' => 'SORT',	// Поле для первой сортировки новостей
 'SORT_BY2' => '',	// Поле для второй сортировки новостей
 'SORT_ORDER1' => 'ASC',	// Направление для первой сортировки новостей
 'SORT_ORDER2' => '',	// Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N',	// Строгая проверка раздела для показа списка
),
 false
);
?>

<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'partners', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d',	// Формат показа даты
	'ADD_SECTIONS_CHAIN' => 'Y',	// Включать раздел в цепочку навигации
	'AJAX_MODE' => 'N',	// Включить режим AJAX
	'AJAX_OPTION_ADDITIONAL' => '',	// Дополнительный идентификатор
	'AJAX_OPTION_HISTORY' => 'N',	// Включить эмуляцию навигации браузера
	'AJAX_OPTION_JUMP' => 'N',	// Включить прокрутку к началу компонента
	'AJAX_OPTION_STYLE' => 'N',	// Включить подгрузку стилей
	'CACHE_FILTER' => 'N',	// Кешировать при установленном фильтре
	'CACHE_GROUPS' => 'N',	// Учитывать права доступа
	'CACHE_TIME' => '36000000',	// Время кеширования (сек.)
	'CACHE_TYPE' => 'A',	// Тип кеширования
	'CHECK_DATES' => 'Y',	// Показывать только активные на данный момент элементы
	'DETAIL_URL' => '',	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
	'DISPLAY_BOTTOM_PAGER' => 'N',	// Выводить под списком
	'DISPLAY_DATE' => 'N',	// Выводить дату элемента
	'DISPLAY_NAME' => 'Y',	// Выводить название элемента
	'DISPLAY_PICTURE' => 'N',	// Выводить изображение для анонса
	'DISPLAY_PREVIEW_TEXT' => 'N',	// Выводить текст анонса
	'DISPLAY_TOP_PAGER' => 'N',	// Выводить над списком
	'FIELD_CODE' => array(	// Поля
		0 => '',
		1 => '',
	),
	'FILTER_NAME' => '',	// Фильтр
	'HIDE_LINK_WHEN_NO_DETAIL' => 'N',	// Скрывать ссылку, если нет детального описания
	'IBLOCK_ID' => 3,	// Код информационного блока
	'IBLOCK_TYPE' => 'mainContent',	// Тип информационного блока (используется только для проверки)
	'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y',	// Включать инфоблок в цепочку навигации
	'INCLUDE_SUBSECTIONS' => 'Y',	// Показывать элементы подразделов раздела
	'MESSAGE_404' => '',	// Сообщение для показа (по умолчанию из компонента)
	'NEWS_COUNT' => 8,	// Количество новостей на странице
	'PAGER_BASE_LINK_ENABLE' => 'N',	// Включить обработку ссылок
	'PAGER_DESC_NUMBERING' => 'N',	// Использовать обратную навигацию
	'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',	// Время кеширования страниц для обратной навигации
	'PAGER_SHOW_ALL' => 'N',	// Показывать ссылку 'Все'
	'PAGER_SHOW_ALWAYS' => 'N',	// Выводить всегда
	'PAGER_TEMPLATE' => '.default',	// Шаблон постраничной навигации
	'PAGER_TITLE' => 'Партнёры',	// Название категорий
	'PARENT_SECTION' => '',	// ID раздела
	'PARENT_SECTION_CODE' => '',	// Код раздела
	'PREVIEW_TRUNCATE_LEN' => '',	// Максимальная длина анонса для вывода (только для типа текст)
	'PROPERTY_CODE' => array(	// Свойства
		0 => 'logotype',
		1 => '',
	),
	'SET_BROWSER_TITLE' => 'N',	// Устанавливать заголовок окна браузера
	'SET_LAST_MODIFIED' => 'N',	// Устанавливать в заголовках ответа время модификации страницы
	'SET_META_DESCRIPTION' => 'N',	// Устанавливать описание страницы
	'SET_META_KEYWORDS' => 'N',	// Устанавливать ключевые слова страницы
	'SET_STATUS_404' => 'N',	// Устанавливать статус 404
	'SET_TITLE' => 'N',	// Устанавливать заголовок страницы
	'SHOW_404' => 'N',	// Показ специальной страницы
	'SORT_BY1' => 'SORT',	// Поле для первой сортировки новостей
	'SORT_BY2' => '',	// Поле для второй сортировки новостей
	'SORT_ORDER1' => 'ASC',	// Направление для первой сортировки новостей
	'SORT_ORDER2' => '',	// Направление для второй сортировки новостей
	'STRICT_SECTION_CHECK' => 'N',	// Строгая проверка раздела для показа списка
	),
	false
);
?>

<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'news.about', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
 'AJAX_MODE' => 'Y', // Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
 'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N', // Учитывать права доступа
 'CACHE_TIME' => '36000000', // Время кеширования (сек.)
 'CACHE_TYPE' => 'A', // Тип кеширования
 'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
 'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
 'DISPLAY_DATE' => 'Y', // Выводить дату элемента
 'DISPLAY_NAME' => 'Y', // Выводить название элемента
 'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
 'FIELD_CODE' => array(0 => ''),
 'FILTER_NAME' => '', // Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 1, // Код информационного блока
 'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
 'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => 3, // Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
 'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
 'PAGER_TITLE' => 'Новости', // Название категорий
 'PARENT_SECTION' => '', // ID раздела
 'PARENT_SECTION_CODE' => '', // Код раздела
 'PREVIEW_TRUNCATE_LEN' => 250, // Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array(0 => 'srcSync2x'),
 'SET_BROWSER_TITLE' => 'N', // Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'N', // Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'N', // Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N', // Устанавливать статус 404
 'SET_TITLE' => 'N', // Устанавливать заголовок страницы
 'SHOW_404' => 'N', // Показ специальной страницы
 'SORT_BY1' => 'ACTIVE_FROM', // Поле для первой сортировки новостей
 'SORT_BY2' => 'SORT', // Поле для второй сортировки новостей
 'SORT_ORDER1' => 'DESC', // Направление для первой сортировки новостей
 'SORT_ORDER2' => 'ASC', // Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
 'COMPONENT_TEMPLATE' => '.default'
),
 false
);
?>

<?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');?>