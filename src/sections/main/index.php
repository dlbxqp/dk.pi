<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Главная страница');
$APPLICATION->AddViewContent('classNameOfTagMain', 'main-page', 1);
?>

 <h1 class="visually-hidden">Профи-инвест</h1>
 <section class="main-page__about">
  <h2 class="visually-hidden">О компании</h2>
  <ul>
<?php
   $APPLICATION->IncludeComponent('bitrix:news.list', 'our_advantage', Array(
    'ACTIVE_DATE_FORMAT' => 'Y-m-d',	// Формат показа даты
    'ADD_SECTIONS_CHAIN' => 'Y',	// Включать раздел в цепочку навигации
    'AJAX_MODE' => 'N',	// Включить режим AJAX
    'AJAX_OPTION_ADDITIONAL' => '',	// Дополнительный идентификатор
    'AJAX_OPTION_HISTORY' => 'N',	// Включить эмуляцию навигации браузера
    'AJAX_OPTION_JUMP' => 'N',	// Включить прокрутку к началу компонента
    'AJAX_OPTION_STYLE' => 'N',	// Включить подгрузку стилей
    'CACHE_FILTER' => 'N',	// Кешировать при установленном фильтре
    'CACHE_GROUPS' => 'N',	// Учитывать права доступа
    'CACHE_TIME' => '36000000',	// Время кеширования (сек.)
    'CACHE_TYPE' => 'A',	// Тип кеширования
    'CHECK_DATES' => 'Y',	// Показывать только активные на данный момент элементы
    'DETAIL_URL' => '',	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    'DISPLAY_BOTTOM_PAGER' => 'N',	// Выводить под списком
    'DISPLAY_DATE' => 'N',	// Выводить дату элемента
    'DISPLAY_NAME' => 'Y',	// Выводить название элемента
    'DISPLAY_PICTURE' => 'Y',	// Выводить изображение для анонса
    'DISPLAY_PREVIEW_TEXT' => 'N',	// Выводить текст анонса
    'DISPLAY_TOP_PAGER' => 'N',	// Выводить над списком
    'FIELD_CODE' => array(	// Поля
     0 => '',
     1 => '',
    ),
    'FILTER_NAME' => '',	// Фильтр
    'HIDE_LINK_WHEN_NO_DETAIL' => 'N',	// Скрывать ссылку, если нет детального описания
    'IBLOCK_ID' => 6,	// Код информационного блока
    'IBLOCK_TYPE' => 'mainContent',	// Тип информационного блока (используется только для проверки)
    'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y',	// Включать инфоблок в цепочку навигации
    'INCLUDE_SUBSECTIONS' => 'Y',	// Показывать элементы подразделов раздела
    'MESSAGE_404' => '',	// Сообщение для показа (по умолчанию из компонента)
    'NEWS_COUNT' => 3,	// Количество новостей на странице
    'PAGER_BASE_LINK_ENABLE' => 'N',	// Включить обработку ссылок
    'PAGER_DESC_NUMBERING' => 'N',	// Использовать обратную навигацию
    'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',	// Время кеширования страниц для обратной навигации
    'PAGER_SHOW_ALL' => 'N',	// Показывать ссылку 'Все'
    'PAGER_SHOW_ALWAYS' => 'N',	// Выводить всегда
    'PAGER_TEMPLATE' => '.default',	// Шаблон постраничной навигации
    'PAGER_TITLE' => 'Партнёры',	// Название категорий
    'PARENT_SECTION' => '',	// ID раздела
    'PARENT_SECTION_CODE' => '',	// Код раздела
    'PREVIEW_TRUNCATE_LEN' => '',	// Максимальная длина анонса для вывода (только для типа текст)
    'PROPERTY_CODE' => array(	// Свойства
     0 => 'srcSet2x',
     1 => 'icon',
     2 => 'className'
    ),
    'SET_BROWSER_TITLE' => 'Y',	// Устанавливать заголовок окна браузера
    'SET_LAST_MODIFIED' => 'N',	// Устанавливать в заголовках ответа время модификации страницы
    'SET_META_DESCRIPTION' => 'Y',	// Устанавливать описание страницы
    'SET_META_KEYWORDS' => 'Y',	// Устанавливать ключевые слова страницы
    'SET_STATUS_404' => 'N',	// Устанавливать статус 404
    'SET_TITLE' => 'Y',	// Устанавливать заголовок страницы
    'SHOW_404' => 'N',	// Показ специальной страницы
    'SORT_BY1' => 'SORT',	// Поле для первой сортировки новостей
    'SORT_BY2' => '',	// Поле для второй сортировки новостей
    'SORT_ORDER1' => 'ASC',	// Направление для первой сортировки новостей
    'SORT_ORDER2' => '',	// Направление для второй сортировки новостей
    'STRICT_SECTION_CHECK' => 'N',	// Строгая проверка раздела для показа списка
   ),
    false
   );
?>
  </ul>
  <div class="main-page__toggle-wrapper">
   <div class="main-page__toggle">
    <button class="js-map-toggle" type="button" aria-label="Переключение между отображением списком и картой"></button>
    <span>Списком</span><span>На карте</span></div>
  </div>
 </section>
 <section class="main-page__projects js-main-projects">
  <h2 class="visually-hidden">Наши проекты</h2>
  <div class="main-page__projects-list js-main-projects-list js-main-projects-map">
   <div class="swiper-container">
    <ul class="swiper-wrapper">
<?
     $APPLICATION->IncludeComponent('bitrix:news.list', 'objects.main', Array(
      'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
      'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
      'AJAX_MODE' => 'N', // Включить режим AJAX
      'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
      'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
      'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
      'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
      'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
      'CACHE_GROUPS' => 'N', // Учитывать права доступа
      'CACHE_TIME' => '36000000', // Время кеширования (сек.)
      'CACHE_TYPE' => 'A', // Тип кеширования
      'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
      'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
      'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
      'DISPLAY_DATE' => 'Y', // Выводить дату элемента
      'DISPLAY_NAME' => 'Y', // Выводить название элемента
      'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
      'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
      'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
      'FIELD_CODE' => array(0 => ''),
      'FILTER_NAME' => '', // Фильтр
      'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
      'IBLOCK_ID' => 2, // Код информационного блока
      'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
      'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
      'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
      'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
      'NEWS_COUNT' => 9, // Количество новостей на странице
      'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
      'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
      'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
      'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
      'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
      'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
      'PAGER_TITLE' => 'Новости', // Название категорий
      'PARENT_SECTION' => '', // ID раздела
      'PARENT_SECTION_CODE' => '', // Код раздела
      'PREVIEW_TRUNCATE_LEN' => 250, // Максимальная длина анонса для вывода (только для типа текст)
      'PROPERTY_CODE' => array(0 => 'srcSync2x'),
      'SET_BROWSER_TITLE' => 'N', // Устанавливать заголовок окна браузера
      'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
      'SET_META_DESCRIPTION' => 'N', // Устанавливать описание страницы
      'SET_META_KEYWORDS' => 'N', // Устанавливать ключевые слова страницы
      'SET_STATUS_404' => 'N', // Устанавливать статус 404
      'SET_TITLE' => 'N', // Устанавливать заголовок страницы
      'SHOW_404' => 'N', // Показ специальной страницы
      'SORT_BY1' => 'SORT', // Поле для первой сортировки новостей
      'SORT_BY2' => '', // Поле для второй сортировки новостей
      'SORT_ORDER1' => 'ASC', // Направление для первой сортировки новостей
      'SORT_ORDER2' => '', // Направление для второй сортировки новостей
      'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
      'COMPONENT_TEMPLATE' => '.default'
     ),
      false
     );
?>
    </ul>
   </div>
   <div class="main-page__map-wrapper js-main-map-wrapper" id="main-page-map"></div>
  </div>
 </section>

<?
$APPLICATION->IncludeComponent('bitrix:news.list', 'news.main', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
 'AJAX_MODE' => 'Y', // Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
 'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N', // Учитывать права доступа
 'CACHE_TIME' => '36000000', // Время кеширования (сек.)
 'CACHE_TYPE' => 'A', // Тип кеширования
 'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
 'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
 'DISPLAY_DATE' => 'Y', // Выводить дату элемента
 'DISPLAY_NAME' => 'Y', // Выводить название элемента
 'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
 'FIELD_CODE' => array(0 => ''),
 'FILTER_NAME' => '', // Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 1, // Код информационного блока
 'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
 'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => 3, // Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
 'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
 'PAGER_TITLE' => 'Новости', // Название категорий
 'PARENT_SECTION' => '', // ID раздела
 'PARENT_SECTION_CODE' => '', // Код раздела
 'PREVIEW_TRUNCATE_LEN' => 250, // Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array(0 => 'srcSync2x'),
 'SET_BROWSER_TITLE' => 'N', // Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'N', // Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'N', // Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N', // Устанавливать статус 404
 'SET_TITLE' => 'N', // Устанавливать заголовок страницы
 'SHOW_404' => 'N', // Показ специальной страницы
 'SORT_BY1' => 'ACTIVE_FROM', // Поле для первой сортировки новостей
 'SORT_BY2' => 'SORT', // Поле для второй сортировки новостей
 'SORT_ORDER1' => 'DESC', // Направление для первой сортировки новостей
 'SORT_ORDER2' => 'ASC', // Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
 'COMPONENT_TEMPLATE' => '.default'
),
 false
);
?>


<?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');?>