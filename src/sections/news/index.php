<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Новости');
?>

<div class='page-news'>
 <div class='page-news__categories'>
  <h1><?$APPLICATION->ShowTitle()?></h1>
  <div class="page-news__select js-select">
   <button type="button">
    <span>
     <span class="js-button-text">Категория</span>
     <svg width="9" height="10" aria-hidden="true">
      <use xlink:href="img/sprite.svg#down-arrow"></use>
     </svg>
    </span>
   </button>
   <ul class="hidden">
    <li>Категория
     <svg width="9" height="10" aria-hidden="true">
      <use xlink:href="img/sprite.svg#down-arrow"></use>
     </svg>
    </li>
<?
$APPLICATION->IncludeComponent('bitrix:catalog.section.list', 'news', Array(
 "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
 "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
 "CACHE_GROUPS" => "N",	// Учитывать права доступа
 "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
 "CACHE_TYPE" => "A",	// Тип кеширования
 "COUNT_ELEMENTS" => 'Y',	// Показывать количество элементов в разделе
 "FILTER_NAME" => '',	// Имя массива со значениями фильтра разделов
 "IBLOCK_ID" => 1,	// Инфоблок
 "IBLOCK_TYPE" => 'mainContent',	// Тип инфоблока
 "SECTION_CODE" => '',	// Код раздела
 "SECTION_FIELDS" => array(	// Поля разделов
  0 => "",
  1 => "",
 ),
 "SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
 "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
 "SECTION_USER_FIELDS" => array(	// Свойства разделов
  0 => "",
  1 => "",
  ),
  "SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
  "TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
  "VIEW_MODE" => 'LIST',	// Вид списка подразделов
 ),
 false
);
#
$APPLICATION->IncludeComponent('bitrix:news.list', 'binding.news', Array(
 'ACTIVE_DATE_FORMAT' => 'Y-m-d', // Формат показа даты
 'ADD_SECTIONS_CHAIN' => 'Y', // Включать раздел в цепочку навигации
 'AJAX_MODE' => 'N', // Включить режим AJAX
 'AJAX_OPTION_ADDITIONAL' => '', // Дополнительный идентификатор
 'AJAX_OPTION_HISTORY' => 'N', // Включить эмуляцию навигации браузера
 'AJAX_OPTION_JUMP' => 'N', // Включить прокрутку к началу компонента
 'AJAX_OPTION_STYLE' => 'N', // Включить подгрузку стилей
 'CACHE_FILTER' => 'N', // Кешировать при установленном фильтре
 'CACHE_GROUPS' => 'N', // Учитывать права доступа
 'CACHE_TIME' => '36000000', // Время кеширования (сек.)
 'CACHE_TYPE' => 'A', // Тип кеширования
 'CHECK_DATES' => 'Y', // Показывать только активные на данный момент элементы
 'DETAIL_URL' => '', // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
 'DISPLAY_BOTTOM_PAGER' => 'N', // Выводить под списком
 'DISPLAY_DATE' => 'Y', // Выводить дату элемента
 'DISPLAY_NAME' => 'Y', // Выводить название элемента
 'DISPLAY_PICTURE' => 'Y', // Выводить изображение для анонса
 'DISPLAY_PREVIEW_TEXT' => 'Y', // Выводить текст анонса
 'DISPLAY_TOP_PAGER' => 'N', // Выводить над списком
 'FIELD_CODE' => array(0 => ''),
 'FILTER_NAME' => '', // Фильтр
 'HIDE_LINK_WHEN_NO_DETAIL' => 'N', // Скрывать ссылку, если нет детального описания
 'IBLOCK_ID' => 1, // Код информационного блока
 'IBLOCK_TYPE' => 'mainContent', // Тип информационного блока (используется только для проверки)
 'INCLUDE_IBLOCK_INTO_CHAIN' => 'Y', // Включать инфоблок в цепочку навигации
 'INCLUDE_SUBSECTIONS' => 'Y', // Показывать элементы подразделов раздела
 'MESSAGE_404' => '', // Сообщение для показа (по умолчанию из компонента)
 'NEWS_COUNT' => '', // Количество новостей на странице
 'PAGER_BASE_LINK_ENABLE' => 'N', // Включить обработку ссылок
 'PAGER_DESC_NUMBERING' => 'N', // Использовать обратную навигацию
 'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000', // Время кеширования страниц для обратной навигации
 'PAGER_SHOW_ALL' => 'N', // Показывать ссылку 'Все'
 'PAGER_SHOW_ALWAYS' => 'N', // Выводить всегда
 'PAGER_TEMPLATE' => '.default', // Шаблон постраничной навигации
 'PAGER_TITLE' => 'Новости', // Название категорий
 'PARENT_SECTION' => '', // ID раздела
 'PARENT_SECTION _CODE' => '', // Код раздела
 'PREVIEW_TRUNCATE_LEN' => 0, // Максимальная длина анонса для вывода (только для типа текст)
 'PROPERTY_CODE' => array(0 => 'object'),
 'SET_BROWSER_TITLE' => 'Y', // Устанавливать заголовок окна браузера
 'SET_LAST_MODIFIED' => 'N', // Устанавливать в заголовках ответа время модификации страницы
 'SET_META_DESCRIPTION' => 'Y', // Устанавливать описание страницы
 'SET_META_KEYWORDS' => 'Y', // Устанавливать ключевые слова страницы
 'SET_STATUS_404' => 'N', // Устанавливать статус 404
 'SET_TITLE' => 'Y', // Устанавливать заголовок страницы
 'SHOW_404' => 'N', // Показ специальной страницы
 'SORT_BY1' => 'ACTIVE_FROM', // Поле для первой сортировки новостей
 'SORT_BY2' => 'SORT', // Поле для второй сортировки новостей
 'SORT_ORDER1' => 'DESC', // Направление для первой сортировки новостей
 'SORT_ORDER2' => 'ASC', // Направление для второй сортировки новостей
 'STRICT_SECTION_CHECK' => 'N', // Строгая проверка раздела для показа списка
 'COMPONENT_TEMPLATE' => '.default'
),
 false
);
?>
   </ul>
   <script>
const url = new URL(window.location.href)
const s = url.searchParams.get('s'); //alert('> ' + s)


const a = document.querySelectorAll('.page-news__categories .js-option')
for(let i = a.length - 1; i >= 0; --i){
 if(a[i].getAttribute('data-code') == s){ console.log('> ' + a[i].text);
  document.querySelector('.page-news__categories .js-button-text').innerHTML = a[i].text
  continue
 }
}
   </script>
  </div>
 </div>
 <ul></ul>
 <button class='button' type='button' onClick='changeNumberOfNews()'>Загрузить ещё</button>

 <script>
const growthOfNews = 12
let countOfNews = growthOfNews
let currentSectionOfNews = 0
const divideNewsIntoSections = (s=currentSectionOfNews, i=countOfNews) => {
 let ul = document.querySelector('.page-news__categories').nextElementSibling
 fetch('/src/php/news.php', {
  method: 'post',
  headers: {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'},
  body: `s=${s}&i=${i}`
 })
  .then(function(response) { return response.text() })
  .then(function(text){ ul.innerHTML = text })
  .catch(function(error){ ul.innerHTML = error })
}
divideNewsIntoSections()

const switchOfReload = () => {
 if(countOfNews > document.getElementsByClassName('js-option')[currentSectionOfNews].getAttribute('data-countOfElements') * 1){
  document.querySelector('.button').style.display = 'none'
 } else{
  document.querySelector('.button').style.display = 'flex'
 }

 //alert(countOfNews + ' / ' + document.getElementsByClassName('js-option')[currentSectionOfNews].getAttribute('data-countOfElements') * 1)
}
switchOfReload()

const changeNumberOfNews = () => {
 countOfNews += growthOfNews
 divideNewsIntoSections()
 switchOfReload()

 return countOfNews
}

const changeCurrentSectionOfNews = (idOfNewCurrentSection = 0) => {
 currentSectionOfNews = idOfNewCurrentSection
 countOfNews = growthOfNews
 divideNewsIntoSections()
 switchOfReload()

 return currentSectionOfNews
}
 </script>
</div>

<?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');?>