<?php
$arUrlRewrite = array(
/*
  0 => 
  array(
    'CONDITION' => '#^\\/?\\/mobileapp/jn\\/(.*)\\/.*#',
    'RULE' => 'componentName=$1',
    'ID' => NULL,
    'PATH' => '/bitrix/services/mobileapp/jn.php',
    'SORT' => 1,
  ),
  1 => array(
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 2,
  ),
*/
  10 => array(
    'CONDITION' => '#^/about(/?.*)#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/src/sections/about/index.php',
    'SORT' => 10,
  ),
  11 => array(
    'CONDITION' => '#^/contacts(/?.*)#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/src/sections/contacts/index.php',
    'SORT' => 11,
  ),
  12 => array(
   'CONDITION' => '#^/news/(\d{1,4}).*#',
   'RULE' => 'id=$1',
   'ID' => NULL,
   'PATH' => '/src/sections/news/news/index.php',
   'SORT' => 121,
  ),
  13 => array(
    'CONDITION' => '#^/news/.*#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/src/sections/news/index.php',
    'SORT' => 122,
  ),
  14 => array(
    'CONDITION' => '#^/object/(\d{1,6}).*#',
    'RULE' => 'id=$1',
    'ID' => NULL,
    'PATH' => '/src/sections/object/index.php',
    'SORT' => 13,
  ),
);
?>